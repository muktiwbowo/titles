package com.muktiwibowo.titles.service

import com.muktiwibowo.titles.model.ModelTitle
import retrofit2.Call
import retrofit2.http.GET

interface ServiceApi {
    @GET("posts/")
    fun fetchTitles(): Call<List<ModelTitle>>
}