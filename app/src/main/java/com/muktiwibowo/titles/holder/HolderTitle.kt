package com.muktiwibowo.titles.holder

import androidx.recyclerview.widget.RecyclerView
import com.muktiwibowo.titles.databinding.HolderTitleBinding
import com.muktiwibowo.titles.model.ModelTitle

class HolderTitle(private val binding: HolderTitleBinding): RecyclerView.ViewHolder(binding.root) {
    fun bindTitle(modelTitle: ModelTitle){
        binding.txtTitle.text = "${modelTitle.title}"
    }
}