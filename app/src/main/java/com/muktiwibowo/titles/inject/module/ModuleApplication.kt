package com.muktiwibowo.titles.inject.module

import android.app.Application
import com.muktiwibowo.titles.inject.scope.ScopeApplication
import com.muktiwibowo.titles.repository.RepositoryDatabase
import com.muktiwibowo.titles.service.ServiceApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ModuleApplication {

    @Provides
    @ScopeApplication
    fun provideRetrofit(): ServiceApi{
        return Retrofit.Builder().baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build().create(ServiceApi::class.java)
    }

    @Provides
    @ScopeApplication
    fun provideRepositoryDatabase(application: Application): RepositoryDatabase{
        return RepositoryDatabase.newInstance(application)
    }
}