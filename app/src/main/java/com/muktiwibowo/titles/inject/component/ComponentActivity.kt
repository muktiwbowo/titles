package com.muktiwibowo.titles.inject.component

import com.muktiwibowo.titles.activity.ActivityMain
import com.muktiwibowo.titles.base.BaseActivity
import com.muktiwibowo.titles.inject.module.ModuleActivity
import com.muktiwibowo.titles.inject.scope.ScopeActivity
import dagger.Component

@ScopeActivity
@Component(dependencies = [(ComponentApplication::class)],modules = [(ModuleActivity::class)])
interface ComponentActivity {
    fun inject(baseActivity: BaseActivity)
    fun inject(activityMain: ActivityMain)
}