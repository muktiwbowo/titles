package com.muktiwibowo.titles.inject.component

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import com.muktiwibowo.titles.base.BaseApplication
import com.muktiwibowo.titles.inject.module.ModuleApplication
import com.muktiwibowo.titles.inject.scope.ScopeApplication
import com.muktiwibowo.titles.repository.RepositoryDatabase
import com.muktiwibowo.titles.service.ServiceApi
import dagger.BindsInstance
import dagger.Component

@ScopeApplication
@Component(modules = [(ModuleApplication::class)])
interface ComponentApplication {
    fun inject(baseApplication: BaseApplication)
    fun serviceApi(): ServiceApi
    fun repositoryDatabase(): RepositoryDatabase

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application): Builder
        @BindsInstance
        fun activity(activity: AppCompatActivity): Builder
        fun build(): ComponentApplication
    }
}