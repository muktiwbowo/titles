package com.muktiwibowo.titles.inject.scope

import javax.inject.Scope

@Scope
@MustBeDocumented
@kotlin.annotation.Retention(value = AnnotationRetention.RUNTIME)
annotation class ScopeActivity
