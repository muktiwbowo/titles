package com.muktiwibowo.titles.inject.module

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.muktiwibowo.titles.inject.scope.ScopeActivity
import dagger.Module
import dagger.Provides

@Module
class ModuleActivity(private val activity: AppCompatActivity) {
    @Provides
    @ScopeActivity
    fun provideActivity(): Context = activity
}