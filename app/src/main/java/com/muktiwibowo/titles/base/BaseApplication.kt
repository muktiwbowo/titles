package com.muktiwibowo.titles.base

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.muktiwibowo.titles.inject.component.ComponentApplication
import com.muktiwibowo.titles.inject.component.DaggerComponentApplication

class BaseApplication: MultiDexApplication() {
    companion object{
        lateinit var componentApplication: ComponentApplication
    }

    override fun onCreate() {
        super.onCreate()
        componentApplication = DaggerComponentApplication
                .builder()
                .application(this)
                .activity(AppCompatActivity())
                .build().also { it.inject(this) }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}