package com.muktiwibowo.titles.base

import androidx.appcompat.app.AppCompatActivity
import com.muktiwibowo.titles.inject.component.ComponentActivity
import com.muktiwibowo.titles.inject.component.DaggerComponentActivity

open class BaseActivity: AppCompatActivity() {
    var componentActivity: ComponentActivity? = null
    get() {
        checkNotNull(field){
            return DaggerComponentActivity
                    .builder()
                    .componentApplication(BaseApplication.componentApplication)
                    .build()
                    .also { it.inject(this) }
        }
        return field
    }
}