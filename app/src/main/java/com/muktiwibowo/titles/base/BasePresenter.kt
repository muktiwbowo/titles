package com.muktiwibowo.titles.base

abstract class BasePresenter<T : BaseView> {
    var viewLayer: T? = null

    open fun attachView(view: T){
        viewLayer = view
    }

    open fun isViewAttach(): Boolean{
        return viewLayer != null
    }

    open fun destroyView(){
        viewLayer = null
    }
}