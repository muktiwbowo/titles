package com.muktiwibowo.titles.repository

import android.app.Application
import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.muktiwibowo.titles.model.ModelTitle

@Database(entities = [ModelTitle::class], version = 1)
abstract class RepositoryDatabase: RoomDatabase() {
    companion object{
        fun newInstance(application: Application): RepositoryDatabase{
            return Room.databaseBuilder(application,
                RepositoryDatabase::class.java, "db_title").build()
        }
    }

    abstract fun daoTitle(): DaoTitle

    @Dao
    interface DaoTitle{
        @Query("SELECT * FROM list_title")
        fun getLocalTitle(): List<ModelTitle>

        @Query("SELECT * FROM list_title WHERE title LIKE :title")
        fun searchLocalTitle(title: String?): List<ModelTitle>

        @Insert(onConflict = REPLACE)
        fun insertLocalTitles(titles: List<ModelTitle>)
    }
}