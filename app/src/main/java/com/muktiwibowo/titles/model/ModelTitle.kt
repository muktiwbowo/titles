package com.muktiwibowo.titles.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "list_title")
data class ModelTitle(
        @PrimaryKey
        @SerializedName(value = "id")
        var id: Int,
        @SerializedName(value = "title")
        var title: String?
)