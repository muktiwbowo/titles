package com.muktiwibowo.titles.presenter

import android.util.Log
import com.muktiwibowo.titles.base.BasePresenter
import com.muktiwibowo.titles.model.ModelTitle
import com.muktiwibowo.titles.repository.RepositoryDatabase
import com.muktiwibowo.titles.service.ServiceApi
import com.muktiwibowo.titles.view.ViewMain
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class PresenterMain
@Inject constructor(
    private val serviceApi: ServiceApi,
    private val repositoryDatabase: RepositoryDatabase
) : BasePresenter<ViewMain>() {

    fun getTitles() {
        viewLayer?.showLoading()
        serviceApi.fetchTitles().enqueue(object : Callback<List<ModelTitle>> {
            override fun onResponse(
                call: Call<List<ModelTitle>>,
                response: Response<List<ModelTitle>>
            ) {
                viewLayer?.hideLoading()
                if (response.isSuccessful && !response.body().isNullOrEmpty()) {
                    response.body()?.let {
                        viewLayer?.showTitles(it)
                        insertTitle(it)
                    }
                } else {
                    getLocalTitles()
                    Log.e(PresenterMain::class.simpleName, "onFailed: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<List<ModelTitle>>, t: Throwable) {
                viewLayer?.hideLoading()
                getLocalTitles()
                Log.e(PresenterMain::class.simpleName, "onFailure: ${t.localizedMessage}")
            }
        })
    }

    private fun getLocalTitles() {
        doAsync {
            val titlesResult = repositoryDatabase.daoTitle().getLocalTitle()
            uiThread {
                viewLayer?.showTitles(titlesResult)
            }
        }
    }

    fun searchTitle(title: String?) {
        doAsync {
            val searchResult = repositoryDatabase.daoTitle().searchLocalTitle(title)
            Log.d(PresenterMain::class.java.simpleName, "results: ${searchResult.size}")
            uiThread {
                viewLayer?.showTitles(searchResult)
            }
        }
    }

    private fun insertTitle(titles: List<ModelTitle>) {
        doAsync {
            repositoryDatabase.daoTitle().insertLocalTitles(titles)
        }
    }
}