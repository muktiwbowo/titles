package com.muktiwibowo.titles.activity

import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.SearchView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.muktiwibowo.titles.adapter.AdapterTitle
import com.muktiwibowo.titles.base.BaseActivity
import com.muktiwibowo.titles.databinding.ActivityMainBinding
import com.muktiwibowo.titles.model.ModelTitle
import com.muktiwibowo.titles.presenter.PresenterMain
import com.muktiwibowo.titles.view.ViewMain
import javax.inject.Inject

class ActivityMain : BaseActivity(), ViewMain, SwipeRefreshLayout.OnRefreshListener {
    @Inject
    lateinit var presenterMain: PresenterMain
    lateinit var binding: ActivityMainBinding
    private lateinit var adapterTitle: AdapterTitle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        componentActivity?.inject(this)
        presenterMain.attachView(this)

        initView()
    }

    private fun initView() {
        binding.srlTitle.setOnRefreshListener(this)
        val layoutManager = LinearLayoutManager(this)
        adapterTitle = AdapterTitle()
        binding.rvTitle.layoutManager = layoutManager
        binding.rvTitle.adapter = adapterTitle

        presenterMain.getTitles()

        binding.svTitle.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                presenterMain.searchTitle(query)
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }

        })
    }

    override fun showTitles(titles: List<ModelTitle>) {
        adapterTitle.listTitle.clear()
        adapterTitle.listTitle.addAll(titles)
        adapterTitle.notifyDataSetChanged()
        Log.d(ActivityMain::class.simpleName, "size titles: ${titles.size}")
    }

    override fun showLoading() {
        binding.srlTitle.isRefreshing = true
    }

    override fun hideLoading() {
        binding.srlTitle.isRefreshing = false
    }

    override fun onDestroy() {
        super.onDestroy()
        presenterMain.destroyView()
    }

    override fun onRefresh() {
        adapterTitle.listTitle.clear()
        presenterMain.getTitles()
    }
}