package com.muktiwibowo.titles.view

import com.muktiwibowo.titles.base.BaseView
import com.muktiwibowo.titles.model.ModelTitle

interface ViewMain: BaseView {
    fun showTitles(titles: List<ModelTitle>)
    fun showLoading()
    fun hideLoading()
}