package com.muktiwibowo.titles.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.muktiwibowo.titles.databinding.HolderTitleBinding
import com.muktiwibowo.titles.holder.HolderTitle
import com.muktiwibowo.titles.model.ModelTitle

class AdapterTitle: RecyclerView.Adapter<HolderTitle>() {
    var listTitle: ArrayList<ModelTitle> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderTitle {
        val inflater = LayoutInflater.from(parent.context)
        val binding = HolderTitleBinding.inflate(inflater)
        return HolderTitle(binding)
    }

    override fun onBindViewHolder(holder: HolderTitle, position: Int) {
        holder.bindTitle(listTitle[position])
    }

    override fun getItemCount() = listTitle.size
}