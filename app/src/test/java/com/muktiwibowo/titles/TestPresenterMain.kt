package com.muktiwibowo.titles

import com.muktiwibowo.titles.model.ModelTitle
import com.muktiwibowo.titles.presenter.PresenterMain
import com.muktiwibowo.titles.repository.RepositoryDatabase
import com.muktiwibowo.titles.service.ServiceApi
import com.muktiwibowo.titles.view.ViewMain
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito
import retrofit2.mock.Calls

class TestPresenterMain {
    private val viewMain: ViewMain = mock()
    private val serviceApi: ServiceApi = mock()
    private val repositoryDatabase: RepositoryDatabase = mock()
    private lateinit var presenterMain: PresenterMain

    @Before
    fun setup(){
        presenterMain = PresenterMain(serviceApi, repositoryDatabase)
        presenterMain.attachView(viewMain)
    }

    @Test
    fun testGetTitles(){
        val modelTitle: List<ModelTitle> = mock()
        Mockito.`when`(serviceApi.fetchTitles()).thenReturn(Calls.response(modelTitle))
        presenterMain.getTitles()

        verify(viewMain).showLoading()
        verify(viewMain).showTitles(modelTitle)
        verify(viewMain).hideLoading()
    }

    @After
    fun testDestroy(){
        presenterMain.destroyView()
    }
}